<?php
class Archivo extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function getByLocalizador($localizador){
		$this->db->where('localizador', $localizador); 
		$query=$this->db->get('archivos');
		
		if($query->num_rows())
			return $query->row();
		else
			return FALSE;
	}
	
	function getAll($role,$inicio,$fin){
		if(!$inicio && !$fin){
			$limite='';
		}
		else{
			if(!$inicio)
				$limite=' LIMIT '.$fin;
			else
				$limite=' LIMIT '.$inicio.','.$fin;
		}
		
		/*$this->db->order_by('id','desc');
		$query=$this->db->get('archivos');
		*/
		
		$query=$this->db->query('Select a.*
								 From archivos as a, dominios as d
								 Where a.dominio_id=d.id AND d.role_id='.$role.' 
								 Order By a.id desc '.$limite);
		
		
		if($query->num_rows()){
			foreach($query->result() as $row){
				$datos[$row->id]->id=$row->id;
				$datos[$row->id]->archivo=$row->archivo;
				$datos[$row->id]->localizador=$row->localizador;
				$datos[$row->id]->dominio_id=$row->dominio_id;
			}
			return $datos;
		}
		else
			return FALSE;
	}
	
	function getAllByLink($role,$inicio,$fin,$link){
		if(!$inicio && !$fin){
			$limite='';
		}
		else{
			if(!$inicio)
				$limite=' LIMIT '.$fin;
			else
				$limite=' LIMIT '.$inicio.','.$fin;
		}
		
		$query=$this->db->query('Select a.*
								 From archivos as a, dominios as d
								 Where a.dominio_id=d.id AND d.role_id='.$role." AND a.archivo like '%".$link."%' 
								 Order By a.id desc ".$limite);
		
		
		if($query->num_rows()){
			foreach($query->result() as $row){
				$datos[$row->id]->id=$row->id;
				$datos[$row->id]->archivo=$row->archivo;
				$datos[$row->id]->localizador=$row->localizador;
				$datos[$row->id]->dominio_id=$row->dominio_id;
			}
			return $datos;
		}
		else
			return FALSE;
	}
	
	function countAll($role){ 
		//$query=$this->db->get('archivos');
		$query=$this->db->query('Select a.*
								 From archivos as a, dominios as d
								 Where a.dominio_id=d.id AND d.role_id='.$role);
		return $query->num_rows();
	}
	
	function countAllByLink($role,$archivo){
		//$this->db->like('archivo',$archivo); 
		//$query=$this->db->get('archivos');
		$query=$this->db->query('Select a.*
								 From archivos as a, dominios as d
								 Where a.dominio_id=d.id AND d.role_id='.$role." AND a.archivo like '%".$archivo."%'");
		
		return $query->num_rows();
	}
	
	function insert($datos){
		$this->db->insert('archivos',$datos);
		return $this->db->insert_id();
	}
	
	function update($datos){
		$this->db->where('id',$datos['id']);
		return $this->db->update('archivos',$datos);
	}
	
	function caducados($role){
		$query=$this->db->query('Select a.*, UNIX_TIMESTAMP(a.fecha) as nfecha
								 From archivos as a, dominios as d
								 Where a.dominio_id=d.id AND d.role_id='.$role);
		if($query->num_rows()){
			$tiempo=time();
			foreach($query->result() as $row){
				$check=$tiempo-$row->nfecha;
				if($check>259200){
					$this->db->where('id',$row->id);
					$this->db->delete('archivos');
				}
			}
		}
	}
    
}