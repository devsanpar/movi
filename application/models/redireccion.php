<?php
class Redireccion extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function getByLocalizador($localizador){
		$this->db->where('localizador', $localizador); 
		$query=$this->db->get('urls');
		
		if($query->num_rows())
			return $query->row();
		else
			return FALSE;
	}
	
	function getAll($role,$inicio,$fin){
		
		if(!$inicio && !$fin){
			$limite='';
		}
		else{
			if(!$inicio)
				$limite=' LIMIT '.$fin;
			else
				$limite=' LIMIT '.$inicio.','.$fin;
		}
		
		/*$this->db->order_by('id','desc');
		$query=$this->db->get('archivos');
		*/
		
		$query=$this->db->query('Select u.*
								 From urls as u, dominios as d
								 Where u.dominio_id=d.id AND d.role_id='.$role.' 
								 Order by u.id desc '.$limite);
		
		
		if($query->num_rows()){
			/*foreach($query->result() as $row){
				$datos[$row->id]->id=$row->id;
				$datos[$row->id]->link=$row->link;
				$datos[$row->id]->localizador=$row->localizador;
				$datos[$row->id]->dominio_id=$row->dominio_id;
			}
			return $datos;*/
			return $query->result();
		}
		else
			return FALSE;
	}
	
	function getAllByLink($role,$inicio,$fin,$link){
		if(!$inicio && !$fin){
			$limite='';
		}
		else{
			if(!$inicio)
				$limite=' LIMIT '.$fin;
			else
				$limite=' LIMIT '.$inicio.','.$fin;
		}
		
		
		$query=$this->db->query('Select u.*
								 From urls as u, dominios as d
								 Where u.dominio_id=d.id AND d.role_id='.$role." AND u.link like '%".$link."%'
								 Order By u.id desc".' '.$limite);
		
		
		
		if($query->num_rows()){
			/*foreach($query->result() as $row){
				$datos[$row->id]->id=$row->id;
				$datos[$row->id]->link=$row->link;
				$datos[$row->id]->localizador=$row->localizador;
				$datos[$row->id]->dominio_id=$row->dominio_id;
			}
			return $datos;*/
			return $query->result();
		}
		else
			return FALSE;
	}
	
	function countAll($role){ 
		//$query=$this->db->get('urls');
		$query=$this->db->query('Select u.*
								 From urls as u, dominios as d
								 Where u.dominio_id=d.id AND d.role_id='.$role);
		return $query->num_rows();
	}
	
	function countAllByLink($role,$link){
		//$this->db->like('link',$link); 
		//$query=$this->db->get('urls');
		
		$query=$this->db->query('Select u.*
								 From urls as u, dominios as d
								 Where u.dominio_id=d.id AND d.role_id='.$role." AND u.link like '%".$link."%'");
		
		return $query->num_rows();
	}
	
	function insert($datos){
		$this->db->insert('urls',$datos);
		return $this->db->insert_id();
	}
	
	function update($datos){
		$this->db->where('id',$datos['id']);
		return $this->db->update('urls',$datos);
	}
	
	function caducados($role){
		$query=$this->db->query('Select u.*, UNIX_TIMESTAMP(u.fecha) as nfecha
								 From urls as u, dominios as d
								 Where u.dominio_id=d.id AND d.role_id='.$role);
		
		if($query->num_rows()){
			$tiempo=time();
			foreach($query->result() as $row){
				$check=$tiempo-$row->nfecha;
				if($check>259200){
					$this->db->where('id',$row->id);
					$this->db->delete('urls');
				}
			}
		}
	}
}