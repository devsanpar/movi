<?php
class Dominio extends CI_Model{

	function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	function get($id){
		$this->db->where('id',$id);
		$query=$this->db->get('dominios');
		
		if($query->num_rows()){
			/*$row=$query->row();
			$datos->id=$row->id;
			$datos->nombre=$row->nombre;
			return $datos;*/
			return current($query->result());
		}
		else
			return FALSE;
	}
	
	function getAll($role_id){
		$this->db->order_by('nombre','asc');
		$this->db->where('role_id',$role_id);
		$query=$this->db->get('dominios');
		
		if($query->num_rows()){
			/*foreach($query->result() as $row){
				$datos[$row->id]->id=$row->id;
				$datos[$row->id]->nombre=$row->nombre;
			}
			return $datos;*/
			return $query->result();
		}
		else
			return FALSE;
	}
    
}