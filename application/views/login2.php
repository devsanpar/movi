<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	    <link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/moredirect.css" />
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/prototype/1.6.1/prototype.js"></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.2/scriptaculous.js"></script>
		<script type="text/javascript" src="<?php echo base_url()?>js/moredirect.js"></script>
	</head>
	<body>
		<center>
			<div style="position:relative; width:978px; background-color:#F1F3F2;">
				<div style="position:relative; width:978px; height:123px; background-image:url('<?php echo base_url()?>imagenes/fondo_header.jpg');">
					<div style="position:absolute; top:27px; left:672px;">
						<img border="0" src="<?php echo base_url().'imagenes/logo_movi.png'?>"/>
					</div>
				</div>
				<div id="formulario" style="padding-top:16px; padding-bottom:53px; height:193px;">
					<div id="admin" style='margin: 50px; margin-left: 100px;' align='center'>
						<div id='login' align='center' >
							<form id="formulario_ingresar" action="<?php echo base_url()?>redirecciones/login" method="post">
								<input type="hidden" name="enviar" value="enviar"/>
								<table class='tabla' cellspacing='0' cellpadding='0' width="300" style="border:1px solid #51c6d9; padding:10px;">
									<tr>
										<td style="font-family:verdana; color:#00517a; font-size:14px;">Nombre de Usuario</td>
									</tr>
									<tr>
										<td class='data'><input style="border:none; background-color:#D2E2E5; font-family:verdana; color:#51c6d9; font-size:12px;" type="text" name="nick" value="<?php echo set_value('nick');?>" size="36"/></td>
									</tr>
									<tr>
										<td style="font-family:verdana; color:#00517a; font-size:14px;">Contrase&ntilde;a</td>
									</tr>
									<tr>
										<td class='data'><input style="border:none; background-color:#D2E2E5; font-family:verdana; color:#51c6d9; font-size:12px;" type="password" name="password" size="36"/></td>
									</tr>
									<tr>
										<td>
											<div class="validation" style="width:300px; font-family:verdana; color:#8dc63f;font-size:12px;">
												<?php if(validation_errors() || $login_errors!=''){
													echo '<br/>Nombre de usuario o Contrase&ntilde;a incorrecta';
												  }?>
											</div>
										</td>
									</tr>
									<tr>
										<td>
											<div style="padding-left:105px; padding-top:10px; cursor:pointer;" onClick="document.forms['formulario_ingresar'].submit()">
												<img border="0" src="<?php echo base_url()?>imagenes/boton_login.jpg"/>
											</div>
										</td>
									</tr>
								</table>
								<div style="display:none;">
									<input type="submit" name="envio" value="envio"/>
								</div>
							</form>
						</div>
					</div>
				</div>		
			</div>
		</center>
	</body>
</html>