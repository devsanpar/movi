<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" > 
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<script src="https://www.google.com/jsapi"></script>
		<script>
		  google.load("prototype", "1.7.0.0");
		  google.load("scriptaculous", "1.8.3");
		</script>
		<style type="text/css">
			body {
				margin-right: auto;
				margin-left: auto;
				margin-top: 0px;
				width: 768px;
				height:100%;
				background-image: url("<?php echo base_url()?>/images/landing_page1/fondo_pag.jpg");
				background-repeat: repeat-x;
				background-color: #64C3D7;
			}
			input {
				width: 420px;
				height: 48px;
				border: none;	
				margin-bottom: 50px;
				text-align: center;
				font-size: 14pt;
			}			
		</style>
		<script type="text/javascript" >
			var error_color='#74C043';
			var ok_color='#000000';
		
			function clear_input(obj,defaultText) {
				$(obj).value="";									
			}

			function fill_input(obj,defaultText) {
				if($(obj).value==""){
					$(obj).value=defaultText;
				}													
			}
		
			function forma_check(){	
				var flag=true;
				if($('nombre').value!='' && $('nombre').value!='Nombre'){					
					$('nombre').setStyle({color : ok_color});
		    	}
				else{
					flag=false;
					$('nombre').setStyle({color : error_color});
				}
				
				var x=$('mail').value;
				var dominio = x.split('@');
				var atpos=x.indexOf("@");
				var dotpos=x.lastIndexOf(".");
				if (atpos<1 || dotpos < (atpos+2) || (dotpos+2) >= x.length || x=='Email' || dominio[1]!="gmail.com"){
					$('mail').setStyle({color : error_color});
					flag=false;
				}
				else{
					$('mail').setStyle({color : ok_color});
				}
				if(flag){
					enviar('formulario');			
				}
			}
		
			function enviar(forma){
				new Ajax.Request($(forma).action,
						 {
							method: 'post',
							parameters: $(forma).serialize(), 
							evalScrips: true,
							onComplete: function(request){
								mostar_pop('gracias');	
								$(forma).reset();	
							}
						 } );
			}		 
		
			function mostar_pop(pop){
				Effect.Appear(pop,{ duration: 1.0, beforeStart: ocultar_pop() });
			}

			function ocultar_pop(){
				Effect.Fade('contenido',{ duration: 1.0 });
			}
		</script>
	
	</head>
	<body>
		<div id='fondo' style='position:relative; width:768px; height:1024px; overflow:hidden;'>
			<img src='<?php echo base_url()?>images/landing_page1/fondo.jpg' />
			<div id='contenido'>
				<div style="position: absolute; width: 419px; height: 137px; background-image: url('<?php echo base_url()?>images/landing_page1/titular.png'); top: 200px; left: 65px; background-repeat: no-repeat;"></div>
				<div style="position: absolute; width: 334px; height: 223px; background-image: url('<?php echo base_url()?>images/landing_page1/modem.png'); top: 350px; left: 434px; background-repeat: no-repeat;"></div>
				<div style="position: absolute; width: 357px; height: 241px; background-image: url('<?php echo base_url()?>images/landing_page1/texto.png'); top: 375px; left: 65px; background-repeat: no-repeat;"></div>
				
		
				<div style="position: absolute; top: 700px; left: 175px;" >
					<form id="formulario" method="post" action='<?php echo base_url()?>campus_board/registro_board'>
						<table>
							<tr>
								<td><input type="text"  value="Nombre" id="nombre" name="nombre" onfocus="clear_input(this,'Nombre')" onblur="fill_input(this,'Nombre')"/></td>					
							</tr>
							<tr>
								<td><input type="text" value="Email" id="mail" name="mail" onfocus="clear_input(this,'Email')" onblur="fill_input(this,'Email')"/></td>
							</tr>							
							<tr>
								<td align="center"><div style="cursor: pointer;" onclick="forma_check()" ><img src="<?php echo base_url()?>/images/landing_page1/enviar.png" alt="" /></div> </td>
							</tr>				
						</table>
					</form>
				</div>
			</div>	
			<div id="gracias" style="position: absolute; width: 393px; height: 162px; background-image: url('<?php echo base_url()?>images/landing_page1/gracias.png'); top: 375px; left: 188px; background-repeat: no-repeat; display: none;"></div>	
		</div>
	</body>
</html>