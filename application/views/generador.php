<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/prototype/1.6.1/prototype.js"></script>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/scriptaculous/1.8.2/scriptaculous.js"></script>
		<?php if($skin==2){?>
		    <link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/moredirect.css"/>
			<script type="text/javascript" src="<?php echo base_url()?>js/moredirect.js"></script>
		<?php }
		  if($skin==3){?>
		    <link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/maredirect.css"/>
			<script type="text/javascript" src="<?php echo base_url()?>js/maredirect.js"></script>
		<?php }
		  if($skin==4){?>
		    <link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/miredirect.css"/>
			<script type="text/javascript" src="<?php echo base_url()?>js/miredirect.js"></script>
		<?php }?>
		<title>Acortador Url</title>
		<link href="<?php echo base_url()?>imagenes/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	</head>
	<body>
		<center>
			<div id="general">
				<div id="header">
					<div id="logo">
					</div>
				</div>
				<div id="formulario">
				</div>
				<div id="contenedor_divisor">
					<div id="lista">
					</div>
				</div>
				<div id="buscar">
					<form  id="formulario_buscar" action="#" method="post">
							<div id="lupa1" class="buscar_lupa" style="<?php if($opcion!=1){?>display:none;<?php }?>" onClick="enviar_forma('formulario_buscar','<?php echo base_url()?>redirecciones/buscar','lista',false,false);"></div>
							<div id="lupa2" class="buscar_lupa" style="<?php if($opcion==1){?>display:none;<?php }?>" onClick="enviar_forma('formulario_buscar','<?php echo base_url()?>redirecciones/buscar_archivo','lista',false,false);"></div>
						<div class="buscar_texto">
							<input class="texto" type="text" name="link" value=""/>
						</div>
					</form>
				</div>
				<div id="usuario">
					<div class="usuario_bienvenido">
						Bienvenido,
					</div>
					<br/>
					<div class="usuario_icono">
					</div>
					<div class="usuario_nombre">
						<?php echo $usuario['user']?>
					</div>
					<a href="<?php echo base_url().'redirecciones/logout'?>">
						<div class="usuario_logout">
						</div>
					</a>
				</div>
			</div>
		</center>
		<script>
			cambio(<?php echo $opcion?>);
			function cambio(cambio){
				if(cambio==1){
					open_vista('<?php echo base_url()?>redirecciones/formulario','formulario',false,false);
					open_vista('<?php echo base_url()?>redirecciones/vista','lista',false,false);
				}
				if(cambio==2){
					open_vista('<?php echo base_url()?>redirecciones/formulario_archivo','formulario',false,false);
					open_vista('<?php echo base_url()?>redirecciones/vista_archivo','lista',false,false);
				}
				if(cambio==3){
					<?php if(isset($dominio_id)){?>
						open_vista('<?php echo base_url()?>redirecciones/formulario_archivo_subido/<?php echo $dominio_id.'/'.$localizador?>','formulario',false,false);
						open_vista('<?php echo base_url()?>redirecciones/vista_archivo','lista',false,false);
					<?php }?>
				}
			}
		</script>
	</body>
</html>