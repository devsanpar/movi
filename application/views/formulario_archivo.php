<form id="formulario_generar" action="<?php echo base_url()?>redirecciones/formulario_archivo" method="post" enctype="multipart/form-data">
	<input type="hidden" id="dominio" name="dominio" value=""/>
	<input type="hidden" id="dominio_id" name="dominio_id" value=""/>
	<input type="hidden" id="enviar" name="enviar" value="enviar"/>
	<table cellpadding="5" cellspacing="5">
		<tr>
			<td colspan="2">
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<div id="contenedor_izquierdo">
								
							</div>
						</td>
						<td>
							<div id="contenedor">
								<div class="espacio">
									<center>
										<input class="texto_archivo" size="20" type="file" name="archivo" value=""/>
									</center>	
								</div>
							</div>
						</td>
						<td>
							<div id="contenedor_derecho">
								
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<div id="boton">
					<div class="boton_contenedor" onClick="document.forms['formulario_generar'].submit();">
						<div class="boton_espacio">
							<center>Generar</center>
						</div>
					</div>
				</div>
				<div id="combo_general">
					<div id="titular_contenedor" class="contenedor_cerrado" onClick="abrirCombo();">
						<div id="combo_titulo" class="combo_titulo">
							Seleccion un dominio
						</div>
						<div class="combo_flecha">
						</div>
					</div>
					<div id="combo" class="combo" style="display:none;" onBlur="abrirCombo();">
						<ul>
							<?php foreach($dominios as $row){?>
								<li id="dominio_<?php echo $row->id?>" onClick="cambiarDominio(<?php echo $row->id?>,'<?php echo $row->nombre?>','combo_titulo');";">
									<?php echo $row->nombre?>
								</li>
							<?php }?>
						</ul>
					</div>
				</div>
				<div id="opciones">
					<div id="opcion_1" class="normal" onClick="cambio(1); cambiarBusqueda('lupa',1,2);">
						<div class="texto">
							Link
						</div>
					</div>
					<div class="divisor"> | </div>
					<div id="opcion_2" class="seleccionado">
						<div class="texto">
							Archivo
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
</form>