<br/>
<div id="lista_titular">
</div>
<br/><br/>
<?php if($lista){?>
	<table id="lista" cellpadding="0" cellspacing="0">
		<tr>
			<td class="titulo">Link</td>
			<td class="titulo">Generado</td>
		</tr>
		<?php
		foreach($lista as $row){
			foreach($dominios as $row2){
				if($row2->id == $row->dominio_id)
					$nameDominios=$row2->nombre;
				
			}

			$extra='';
			if(strlen($row->link)>80)
				$extra='...';?>
			<tr>
				<td class="datos"><?php echo substr($row->link,0,80).$extra?></td>
				<td class="datos">
					<a class="rapido" href="http://<?php echo $nameDominios.'/?l='.$row->localizador?>" target="_blank">
						<?php 						
						echo $nameDominios.'/?l='.$row->localizador?>
					</a>
				</td>
			</tr>
		<?php }?>
		<tr>
			<td colspan="2">
				<?php if(($this->uri->segment(3))+$pagina<$total){?>
					<div class="paginacion_siguiente" onClick="open_vista('<?php echo base_url()?>redirecciones/vista/<?php echo ($this->uri->segment(3)+$pagina)?>','lista',false,false);">
						<u>siguiente ></u>
					</div>
				<?php }?>
				<?php if($this->uri->segment(3)){?>
					<div class="paginacion_anterior" onClick="open_vista('<?php echo base_url()?>redirecciones/vista/<?php echo ($this->uri->segment(3)-$pagina)?>','lista',false,false);">
						<u>< anterior</u>
					</div>
				<?php }?>
			</td>
		</tr>
	</table>
<?php }else{?>
	<div id="lista_error">
		No existen elementos.
	</div>
<?php }?>