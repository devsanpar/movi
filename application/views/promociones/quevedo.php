<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta name="keywords" content="movistar,movistar ecuador,internet móvil,internet,  moviles,dias movistar,ofertas moviles,telefonia, lineas,ahorro,planes,descuentos,comprar móvil, telefonos movistar,otecel,pagina de movistar,pda, smart phones, pregago,pospago,mensajes movistar,celulares,celulares nuevos,moviles actiles,tarifas movistar" />
		<meta name="description" content="Movistar Ecuador: Las mejores tarifas para telefonía móvil e internet. Equipos de última tecnología, planes pospago y pregago, promociones y ofertas en servicios." />
		<meta name="generator" content="Movistar Ecuador" />
		<title>Movistar. Compartida, la vida es más.</title>
		<link type="text/css" rel="stylesheet" href="<?php echo base_url()?>css/quevedo.css" />
		<link href="http://www.movistar.com.ec/site/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<script src="http://www.google.com/jsapi"></script>
		<script>
		  google.load("prototype", "1.7.0.0");
		  google.load("scriptaculous", "1.8.3");
		</script>
		<script type="text/javascript">
		  var _gaq = _gaq || [];
		  _gaq.push(['_setAccount', 'UA-17479447-35']);
		  _gaq.push(['_trackPageview']);
		
		  (function() {
		    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		  })();
		
		</script>
	</head>
	<body>
		<center>
		<div id='content'>
			<div id='logo'>
			<a href='http://www.movistar.com.ec/site'>
				<img src="<?php echo base_url()?>imagenes/promociones/logo.png" border="0" />
			</a>
			</div>
			<div id='recuadro'>
				<div style='position:absolute; top:48px; left:49px;'>
					<img src="<?php echo base_url()?>imagenes/promociones/recuadro.jpg" border="0" />
				</div>

				<div style='position:absolute; top:398px; left:48px;'>
					<img src="<?php echo base_url()?>imagenes/promociones/meses.jpg" border="0" />
				</div>
				<div style='position:absolute; top:540px; left:415px;'>
					<img src="<?php echo base_url()?>imagenes/promociones/logos-redes.jpg" border="0" />
					<div id='' style='position:absolute; top:15px; left:0px; width:30px; height:30px; cursor:pointer;' onclick="location.href='https://www.facebook.com/MovistarECU';"></div>
					<div id='' style='position:absolute; top:15px; right:10px; width:30px; height:30px; cursor:pointer;'  onclick="location.href='http://twitter.com/#!/movistarec';"></div>
				</div>
				<div style='position:absolute; top:630px; left:50px;'>
					<img src="<?php echo base_url()?>imagenes/promociones/condiciones.jpg" border="0" />
				</div>
			</div>
		</div>
		</center>
	</body>
</html>