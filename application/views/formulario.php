<form id="formulario_generar" method="post">
	<input type="hidden" id="dominio" name="dominio" value=""/>
	<input type="hidden" id="dominio_id" name="dominio_id" value=""/>
	<input type="hidden" id="enviar" name="enviar" value="enviar"/>
	<table cellpadding="5" cellspacing="5">
		<tr>
			<td colspan="2">
				
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<div id="contenedor_izquierdo">
								
							</div>
						</td>
						<td>
							<div id="contenedor">
								<div class="espacio">
									<input class="texto" type="text" id="link" name="link" value="Ingrese aquí su link" size="30" onClick="$('link').value='';"/>
								</div>
							</div>
						</td>
						<td>
							<div id="contenedor_derecho">
								
							</div>
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<div id="boton">
					<div class="boton_contenedor" onClick="enviar_forma('formulario_generar','<?php echo base_url()?>redirecciones/formulario','formulario',false,false)">
						<div class="boton_espacio">
							<center>Generar</center>
						</div>
					</div>
				</div>
				<div id="combo_general">
					<div id="titular_contenedor" class="contenedor_cerrado" onClick="abrirCombo();">
						<div id="combo_titulo" class="combo_titulo">
							Seleccion un dominio
						</div>
						<div class="combo_flecha">
						</div>
					</div>
					<div id="combo" class="combo" style="display:none;" onBlur="abrirCombo();">
						<ul>
							<?php foreach($dominios as $row){?>
								<li id="dominio_<?php echo $row->id?>" onClick="cambiarDominio(<?php echo $row->id?>,'<?php echo $row->nombre?>','combo_titulo');";">
									<?php echo $row->nombre?>
								</li>
							<?php }?>
						</ul>
					</div>
				</div>
				<div id="opciones">
					<div id="opcion_1" class="seleccionado">
						<div class="texto">
							Link
						</div>
					</div>
					<div class="divisor"> | </div>
					<div id="opcion_2" class="normal" onClick="cambio(2); cambiarBusqueda('lupa',2,2);">
						<div class="texto">
							Archivo
						</div>
					</div>
				</div>
			</td>
		</tr>
	</table>
</form>