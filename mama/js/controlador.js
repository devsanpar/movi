(function(angular) {
    'use strict';
    var templates = '/mama/templates/';
    //var templates = 'templates/';

    var json = '/mama/db.json';
    //var json = 'db.json';    

    var myapp = angular.module(
        'mainApp', 
        ['ngSanitize', 'ui.router', 'ngAnimate', 'angularytics']

    ).config(['$stateProvider', function ($stateProvider) {

        var item= {
            url: '/promo-metro',
            templateUrl: templates+'promo-metro.html',
            controller: 'promo-metro'

        };


        $stateProvider
           .state('promo-metro', item);


    }]).config(function ($urlRouterProvider) { 

        $urlRouterProvider.otherwise('/promo-metro');

    }).config(function(AngularyticsProvider) {
        AngularyticsProvider.setEventHandlers(['Console', 'GoogleUniversal']);
    }).run(function(Angularytics) {
        Angularytics.init();
    }).factory('DataBase', function ($rootScope, $http) {

        var nukeService = {};
        nukeService.data = {};

        nukeService.getData = function() {

            $http.get(json)

                .success(function(data) {

                    nukeService.data.d = data;

                });

            return nukeService.data;
        };

        return nukeService;

    }).filter('custom', function() {

        return function(input, search) {
            if (!input) return input;
            if (!search) return input;

            var expected = ('' + search).toLowerCase();

            var result = {};

            angular.forEach(input, function(value, key) {

              var actual = ('' + value).toLowerCase();

              if (actual.indexOf(expected) !== -1) {

                result[key] = value;

              }

            });

            return result;

        }

    }).controller('promo-metro', function($scope, $location, $compile, DataBase){

        $scope.key = $location.search().eq;

        var DB = DataBase.getData();
        $scope.DB = DB;

        $scope.showdiv = function(key){

            ga('send', 'event', 'Promo-metro-mama', 'Me interesa', key);
            $scope.key = key;
            var item = DB.d[1][key];
            $scope.templateURL = templates+'form.html';

        };

        $scope.trackform = function(key){
            //ga('send', 'event', 'Promo-metro-mama', 'Form - boton enviar', key);
        };

    }).directive('scrollOnClick', function() {

        return {

            restrict: 'A',

            link: function(scope, $elm, attrs) {

                $elm.on('click', function(attrs) {

                    var tar = $elm.attr('tar');  

                    $('html, body').animate({scrollTop: $(tar).offset().top}, "slow");

                });

            }

        }

    });

})(window.angular);