var cmb=0;

function cambiarDominio(id,nombre,titular){
	$(titular).innerHTML=nombre;
	$('dominio').value=nombre;
	$('dominio_id').value=id;
	$('combo').hide();
	$('titular_contenedor').className='contenedor_cerrado';
	cmb=0;
}

function abrirCombo(){
	if(cmb){
		$('combo').hide();
		$('titular_contenedor').className='contenedor_cerrado';
	}
	else{
		$('combo').show();
		$('titular_contenedor').className='contenedor_abierto';
	}
	cmb=(cmb*-1)+1;
}

function open_vista(url,contenedor,url2,contenedor2){	
	if(url==''){
		document.getElementById(contenedor).innerHTML="";
	}
	new Ajax.Updater(contenedor, url,
		{
			evalScripts:true,
			onLoading: function(request){ 
				document.getElementById(contenedor).innerHTML='<center><div style="padding-top:100px"><img border="0" src="../imagenes/load.gif"/></div></center>';
			},
			onComplete: function(request){
				open_vista(url2,contenedor2);
			}
		});
}

function enviar_forma(forma,url,contenedor,url2,contenedor2){
	new Ajax.Updater(contenedor, url,
		{
			evalScripts:true,
			method: 'post',
			parameters: $(forma).serialize(), 
			onLoading: function(request){ 
				document.getElementById(contenedor).innerHTML='<center><div style="padding-top:100px"><img border="0" src="../imagenes/load.gif"/></div></center>';
			},
			onComplete: function(request){
				open_vista(url2,contenedor2,'FALSE','FALSE');
			}
		});
}

function cambiarOpcion(contenedor,actual,total){
	for(i=1;i<=total;i++){
		if(i==actual)
			$(contenedor+i).className="seleccionado";
		else
			$(contenedor+i).className="normal";
	}
}

function cambiarBusqueda(contenedor,actual,total){
	for(i=1;i<=total;i++){
		if(i==actual)
			$(contenedor+i).show();
		else
			$(contenedor+i).hide();
	}
}